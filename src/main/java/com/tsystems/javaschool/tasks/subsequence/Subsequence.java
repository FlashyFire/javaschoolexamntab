package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null)
            throw new IllegalArgumentException("x");
        if (y == null)
            throw new IllegalArgumentException("y");
        int j = 0;
        for (int i = 0; i < x.size(); i++) {
            if (j>=y.size())
                return false;
            while (x.get(i) != y.get(j)){
                j++;
                if (j>=y.size())
                    return false;
            }
            j++;
        }
        return true;
    }
}
