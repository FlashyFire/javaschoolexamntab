package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement == "")
            return null;
        try {
            String[] tokens = getTokens(statement);
            String[] rpn = convertInfixToRPN(tokens);
            Double res = evalRPN(rpn);
            if (Double.isInfinite(res) || Double.isNaN(res))
                return null;
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getNumberInstance(Locale.ENGLISH);
            formatter.applyPattern("#0.####");
            return formatter.format(res);
        } catch (Exception e){
            return null;
        }
    }

    private String[] getTokens(String statement){
        Set<Character> operations = new HashSet<>();
        operations.add('*');
        operations.add('/');
        operations.add('+');
        operations.add('-');
        operations.add('(');
        operations.add(')');
        StringBuilder sb = new StringBuilder();
        List<String> tokens = new ArrayList<>();
        for (Character c : statement.toCharArray()) {
            if (operations.contains(c)){
                String s = sb.toString().trim();
                if (s != null && !"".equals(s)){
                    tokens.add(s);
                }
                sb.setLength(0);
                tokens.add(c.toString());
            }
            else sb.append(c);
        }
        String s = sb.toString().trim();
        if (s != null && !"".equals(s)){
            tokens.add(s);
        }
        return tokens.toArray(new String[0]);
    }
    boolean isNumber(String str) {
        try {
            Double.valueOf(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    String[] convertInfixToRPN(String[] infixNotation) {
        Map<String, Integer> prededence = new HashMap<>();
        prededence.put("/", 5);
        prededence.put("*", 5);
        prededence.put("+", 4);
        prededence.put("-", 4);
        prededence.put("(", 0);

        Queue<String> Q = new LinkedList<>();
        Stack<String> S = new Stack<>();

        for (String token : infixNotation) {
            if ("(".equals(token)) {
                S.push(token);
                continue;
            }

            if (")".equals(token)) {
                while (!"(".equals(S.peek())) {
                    Q.add(S.pop());
                }
                S.pop();
                continue;
            }
            // an operator
            if (prededence.containsKey(token)) {
                while (!S.empty() && prededence.get(token) <= prededence.get(S.peek())) {
                    Q.add(S.pop());
                }
                S.push(token);
                continue;
            }

            if (isNumber(token)) {
                Q.add(token);
                continue;
            }

            throw new IllegalArgumentException("Invalid input");
        }
        // at the end, pop all the elements in S to Q
        while (!S.isEmpty()) {
            Q.add(S.pop());
        }

        return Q.toArray(new String[0]);
    }

    Double calculate(Double left, Double right, String operator) {
        if ("+".equals(operator)) {
            return left + right;
        }

        if ("-".equals(operator)) {
            return left - right;
        }

        if ("*".equals(operator)) {
            return left * right;
        }

        if ("/".equals(operator)) {
            return left / right;
        }

        throw new IllegalArgumentException("Invalid input");

    }

    Double evalRPN(String[] tokens) {
        Stack<Double> stack = new Stack<>();
        Set<String> operators = new HashSet<>();
        operators.add("+");
        operators.add("-");
        operators.add("*");
        operators.add("/");

        String cur;

        for (int i = 0; i < tokens.length; i++) {
            cur = tokens[i];
            if (operators.contains(cur)) {
                Double right = stack.pop();
                Double left = stack.pop();
                stack.push(calculate(left, right, cur));
            } else {
                stack.push(Double.valueOf(cur));
            }
        }
        return stack.pop();
    }

}