package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int levels = getLevels(inputNumbers.size());
        if (levels == 0) throw new CannotBuildPyramidException();
        try {
            inputNumbers.sort(Comparator.naturalOrder());
        }catch (Exception e){
            throw new CannotBuildPyramidException();
        }
        int[][] res = new int[levels][2*levels -1];
        int x = levels - 1;
        int y = 0;
        int index = 0;
        for (int level = 1; level <= levels; level++) {
            for (int i = 0; i < level; i++) {
                res[y][x + i * 2] = inputNumbers.get(index++);
            }
            x--;
            y++;
        }
        return res;
    }

    int getLevels(int count) {
        int n = 1;
        while (count > 0) {
            count -= n;
            n++;
        }
        if (count < 0)
            return 0;
        return n - 1;
    }
}